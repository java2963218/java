package org.tpjava2324.longaing_guy.serie5;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Exo14 {
    public static void main(String[] args) {
        // Créer une liste de chaînes de caractères
        List<String> words = Arrays.asList("one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve");

        // Afficher la liste des mots
        System.out.println("Liste des mots :");
        for (String word : words) {
            System.out.println(word);
        }

        // Afficher les mots en majuscules
        System.out.println("\nListe des mots en majuscules :");
        for (String word : words) {
            System.out.println(word.toUpperCase());
        }

        // Premières lettres des éléments en majuscules, sans doublons
        System.out.println("\nPremières lettres en majuscules, sans doublons :");
        words.stream()
             .map(word -> word.substring(0, 1).toUpperCase())
             .distinct()
             .forEach(System.out::println);

        // Longueurs des éléments, sans doublons
        System.out.println("\nLongueurs des éléments sans doublons :");
        words.stream()
             .map(String::length)
             .distinct()
             .forEach(System.out::println);

        // Afficher le nombre d'éléments du flux
        long count = words.stream().count();
        System.out.println("\nNombre d'éléments dans le flux : " + count);

        // Nombre d'éléments avec une longueur paire
        long evenLengthCount = words.stream()
                                    .filter(word -> word.length() % 2 == 0)
                                    .count();
        System.out.println("Nombre d'éléments avec une longueur paire : " + evenLengthCount);

        // Chaîne la plus longue
        int maxLength = words.stream()
                             .max(Comparator.comparingInt(String::length))
                             .map(String::length)
                             .orElse(0);
        System.out.println("Chaîne la plus longue : " + maxLength);

        // Longueur impaire en majuscules
        List<String> oddLengthWords = words.stream()
                                           .filter(word -> word.length() % 2 != 0)
                                           .map(String::toUpperCase)
                                           .collect(Collectors.toList());
        System.out.println("Longueur impaire en majuscules : " + oddLengthWords);

        // Concaténation 5 caractères ou moins
        String concatenated = words.stream()
                                   .filter(word -> word.length() <= 5)
                                   .sorted()
                                   .collect(Collectors.joining(", ", "{", "}"));
        System.out.println("Concaténation 5 caractères ou moins : " + concatenated);

        // Table de hachage 
        Map<Integer, List<String>> lengthMap = words.stream()
                                                    .collect(Collectors.groupingBy(String::length));
        System.out.println("Table de hachage : " + lengthMap);

        // Table de hachage avec les premières lettres
        Map<String, String> firstLetterMap = words.stream()
            .collect(Collectors.groupingBy(word -> word.substring(0, 1),
                                           Collectors.joining(", ")));
        System.out.println("Table de hachage des premières lettres : " + firstLetterMap);
    
    }

}
