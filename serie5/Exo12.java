package org.tpjava2324.longaing_guy.serie5;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;

public class Exo12 {
    public static void main(String[] args) {

        List<String> words = new ArrayList<>(List.of(
                "one", "two", "three", "four", "five",
                "six", "seven", "eight", "nine", "ten",
                "eleven", "twelve"
        ));
        System.out.println("Liste :");
        for (String word : words) {
            System.out.println(word);
        }

        // Suppression
        words.removeIf(word -> word.length() % 2 == 0);
        System.out.println("\nsuppression des mots de longueur paire:");
        for (String word : words) {
            System.out.println(word);
        }

        
        // majuscule commençant par une voyelle
        Predicate<String> voy = word -> {
            String lowerCaseWord = word.toLowerCase();
            return lowerCaseWord.startsWith("a") || lowerCaseWord.startsWith("e") ||
            lowerCaseWord.startsWith("i") || lowerCaseWord.startsWith("o") ||
            lowerCaseWord.startsWith("u");
        };
        words.replaceAll(word -> voy.test(word) ? word.toUpperCase() : word);
        System.out.println("\nmise en majuscule des mots commençant par une voyelle:");
        for (String word : words) {
            System.out.println(word);
        }
        
        //majuscule
        words.replaceAll(String::toUpperCase);
        System.out.println("\nMise en majuscule:");
        for (String word : words) {
            System.out.println(word);
        }

        // Tri
        Comparator<String> compareByLength = Comparator.comparingInt(String::length);
        words.sort(compareByLength.thenComparing(Comparator.naturalOrder()));

        System.out.println("\nListe de tri :");
        for (String word : words) {
            System.out.println(word);
        }
    }
}
