package org.tpjava2324.longaing_guy.serie5;

import java.util.*;

public class Exo13 {
    public static void main(String[] args) {
        List<String> words = Arrays.asList(
                "one", "two", "three", "four", "five",
                "six", "seven", "eight", "nine", "ten",
                "eleven", "twelve"
        );

        //liste
        System.out.println("Liste des mots :");
        for (String word : words) {
            System.out.println(word);
        }

        Map<Integer, String> number = new HashMap<>();
        for (int i = 0; i < words.size(); i++) {
            
            number.put(i + 1, words.get(i));
        }

        System.out.println("\nCarte de hachage : " + number );

        for (Map.Entry<Integer, String> entry : number.entrySet()) {
            number.put(entry.getKey(), entry.getValue().toUpperCase());
        }

        System.out.println("\nMise en majuscules : " + number);

        Map<Integer, String> sortedMap = new TreeMap<>(number);

        System.out.println("\nCarte triée : " + sortedMap);
    }
}
