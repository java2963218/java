package org.tpjava2324.longaing_guy.serie7;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Exo16 {
    public static List<String> readLinesFrom(String urlString) {
        List<String> lines = new ArrayList<>();
        try {
            URL url = new URL(urlString);
            try (InputStream is = url.openStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8))) {

                String line;
                int lineCount = 0;
                while ((line = reader.readLine()) != null) {
                    lineCount++;

                    if (lineCount <= 70)
                        continue;
                    lines.add(line);
                }

                if (lines.size() > 322) {
                    lines = lines.subList(0, lines.size() - 322);
                } else {

                    lines.clear();
                }
            }
        } catch (IOException e) {
            System.out.println("Erreur " + e.getMessage());
            return null;
        }
        return lines;
    }

    private static final BiFunction<String, String, Long> countOccurrencesInLine = (line, word) -> {

        final String normalizedLine = line.toLowerCase();
        final String normalizedWord = word.toLowerCase();
        final Pattern pattern = Pattern.compile("\\b" + Pattern.quote(normalizedWord) + "\\b",
                Pattern.UNICODE_CHARACTER_CLASS);

        return pattern.matcher(normalizedLine).results().count();
    };

    public static long countWordOccurrences(List<String> lines, String word) {
        return lines.stream()
                .mapToLong(line -> countOccurrencesInLine.apply(line, word))
                .sum();
    }

    // Q4
    public static Stream<Character> lineToCharacterStream(String line) {
        return line.chars().mapToObj(c -> (char) c);
    }

    // Q5
    public static Stream<Character> linesToCharacterStream(List<String> lines) {
        return lines.stream().flatMap(Exo16::lineToCharacterStream);
    }

    // Q6
    public static List<Character> uniqueSortedCharacters(Stream<Character> charactersStream) {
        return charactersStream
                .distinct()
                .sorted()
                .collect(Collectors.toList());
    }

    public static List<Character> nonLetterCharacters(List<Character> characters) {
        return characters.stream()
                .filter(c -> !Character.isLetter(c))
                .collect(Collectors.toList());
    }

    public static void main(String[] args) {
        List<String> lines = readLinesFrom("https://ia600502.us.archive.org/10/items/germinal05711gut/7germ10.txt");
        if (lines != null && !lines.isEmpty()) {
            System.out.println("Nombre de lignes dans le texte de Germinal: " + lines.size());

            long nonEmptyLinesCount = lines.stream()
                    .filter(line -> !line.trim().isEmpty())
                    .count();
            System.out.println("Nombre de lignes de texte non vides dans Germinal: " + nonEmptyLinesCount);

            long bonjourCount = countWordOccurrences(lines, "bonjour");
            System.out.println("Le mot 'bonjour' apparaît " + bonjourCount + " fois dans Germinal.");

            // Q5
            Stream<Character> charactersStream = linesToCharacterStream(lines);

            // Q6
            List<Character> uniqueSortedChars = uniqueSortedCharacters(charactersStream);

            List<Character> nonLetters = nonLetterCharacters(uniqueSortedChars);

            System.out.println("Caractères non-lettres trouvés dans Germinal: " + nonLetters);

            // Q7
            long totalWordCount = lines.stream()
                    .flatMap(line -> Arrays.stream(line.split("\\s+")))
                    .count();

            long distinctWordCount = lines.stream()
                    .flatMap(line -> Arrays.stream(line.split("\\s+")))
                    .distinct()
                    .count();

            System.out.println("Nombre total de mots: " + totalWordCount);
            System.out.println("Nombre de mots différents: " + distinctWordCount);

            // Q8
            Optional<String> longestWordOptional = lines.stream()
                    .flatMap(line -> Arrays.stream(line.split("\\s+")))
                    .max(Comparator.comparingInt(String::length));

            String longestWord = longestWordOptional.orElse("");
            System.out
                    .println("Le mot le plus long: " + longestWord + " ,avec une longueur de: " + longestWord.length());

            // Q9
            long countOfLongestWords = lines.stream()
                    .flatMap(line -> Arrays.stream(line.split("\\s+")))
                    .filter(word -> word.length() == longestWord.length())
                    .count();

            System.out.println("Nombre de mots de cette longueur: " + countOfLongestWords);

            // Q10
            Map<String, Long> wordFrequencies = lines.stream()
                    .flatMap(line -> Arrays.stream(line.split("\\s+")))
                    .filter(word -> !word.toLowerCase().contains("e"))
                    .collect(Collectors.groupingBy(String::toLowerCase, Collectors.counting()));

            String mostFrequentNonEWord = Collections.max(wordFrequencies.entrySet(), Map.Entry.comparingByValue())
                    .getKey();

            System.out.println("Le mot le plus fréquent sans la lettre 'e' est: " + mostFrequentNonEWord);

        } else {
            System.out.println("Échec");
        }

    }
}
