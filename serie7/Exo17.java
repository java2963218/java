package org.tpjava2324.longaing_guy.serie7;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;

public class Exo17 {
    public static List<String> readLinesFrom(String urlString) {
        List<String> lines = new ArrayList<>();
        try {
            URL url = new URL(urlString);
            try (InputStream is = url.openStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8))) {

                String line;
                int lineCount = 0;
                while ((line = reader.readLine()) != null) {
                    lineCount++;

                    if (lineCount <= 70)
                        continue;
                    lines.add(line);
                }

                if (lines.size() > 322) {
                    lines = lines.subList(0, lines.size() - 322);
                } else {
                    lines.clear();
                }
            }
        } catch (IOException e) {
            System.out.println("Erreur " + e.getMessage());
            return null;
        }
        return lines;
    }

    public static void main(String[] args) {
        List<String> lines = readLinesFrom("https://ia600502.us.archive.org/10/items/germinal05711gut/7germ10.txt");

        if (lines != null && !lines.isEmpty()) {

            Map<Integer, Long> wordLengthHistogram = lines.stream()
                    .flatMap(line -> Arrays.stream(line.split("\\s+")))
                    .filter(word -> word.length() >= 2)
                    .collect(Collectors.groupingBy(String::length, Collectors.counting()));

            List<Map.Entry<Integer, Long>> sortedEntries = wordLengthHistogram.entrySet().stream()
                    .sorted(Map.Entry.comparingByKey())
                    .collect(Collectors.toList());


            long totalWords = sortedEntries.stream()
                    .mapToLong(Map.Entry::getValue)
                    .sum();


            long medianIndex = totalWords / 2;
            long wordCount = 0;
            int medianWordLength = 0;

            for (Map.Entry<Integer, Long> entry : sortedEntries) {
                wordCount += entry.getValue();
                if (wordCount >= medianIndex) {
                    medianWordLength = entry.getKey();
                    break;
                }
            }
            System.out.println("Type de la table de hachage : " + wordLengthHistogram.getClass().getSimpleName());
            System.out.println("Nombre de paires clés/valeurs : " + wordLengthHistogram.size());
            wordLengthHistogram
                    .forEach((key, value) -> System.out.println("Longueur : " + key + ", Nombre de mots : " + value));

            long totalWordCount = wordLengthHistogram.values().stream().mapToLong(Long::longValue).sum();
            long medianCount = totalWordCount / 2;
            long currentCount = 0;

            for (Map.Entry<Integer, Long> entry : wordLengthHistogram.entrySet().stream()
                    .sorted(Map.Entry.comparingByKey()).collect(Collectors.toList())) {
                currentCount += entry.getValue();
                if (currentCount >= medianCount) {
                    medianWordLength = entry.getKey();
                    break;
                }
            }

            System.out.println("La longueur de mot médiane est : " + medianWordLength);
            System.out.println("Nombre de mots ayant la longueur médiane (" + medianWordLength + ") : " +
                    wordLengthHistogram.get(medianWordLength));
        } else {
            System.out.println("Échec.");
        }
    }
}