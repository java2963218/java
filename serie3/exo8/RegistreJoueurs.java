package org.tpjava2324.longaing_guy.serie3.exo8;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class RegistreJoueurs {
    private Map<Integer, Set<String>> tableJoueurs;

    public RegistreJoueurs() {
        this.tableJoueurs = new HashMap<>();
    }

    public void addJoueur(Joueur joueur) {
        int decennie = (joueur.getAnneeNaissance() / 10) * 10;
        Set<String> joueurs = tableJoueurs.computeIfAbsent(decennie, k -> new HashSet<>());
        joueurs.add(joueur.getNom());
    }

    public Set<String> get(int decennie) {
        return tableJoueurs.getOrDefault(decennie, new HashSet<>());
    }

    public int count(int decennie) {
        return get(decennie).size();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        tableJoueurs.forEach((decennie, joueurs) -> {
            sb.append("Décennie ").append(decennie).append(": ");
            joueurs.forEach(nom -> sb.append(nom).append(" "));
            sb.append("\n");
        });
        return sb.toString();
    }
}
