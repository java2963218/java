package org.tpjava2324.longaing_guy.serie3.exo8;

public class Joueur {
    private String nom;
    private int anneeNaissance;

    public Joueur(String nom, int anneeNaissance) {
        this.nom = nom;
        this.anneeNaissance = anneeNaissance;
    }

    public String getNom() {
        return nom;
    }

    public int getAnneeNaissance() {
        return anneeNaissance;
    }

    @Override
    public String toString() {
        return "Nom: " + nom + ", Année: " + anneeNaissance;
    }
}
