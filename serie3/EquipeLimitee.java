package org.tpjava2324.longaing_guy.serie3;

public class EquipeLimitee extends Equipe {
    private int nombreMax;

    public EquipeLimitee(int nombreMax) {
        super();
        this.nombreMax = nombreMax;
    }

    @Override
    public boolean addJoueur(Joueur joueur) {
        if (getNombreJoueurs() < nombreMax) {
            return super.addJoueur(joueur);
        } 
        else {
            System.out.println("L'équipe est déjà au complet. Impossible d'ajouter le joueur : " + joueur.getNom());
            return false;
        }
    }
}
