package org.tpjava2324.longaing_guy.serie3;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class Equipe {
    private Set<Joueur> joueurs;

    public Equipe() {
        joueurs = new HashSet<>();
    }

    public boolean addJoueur(Joueur joueur) {
        return joueurs.add(joueur);
    }

    public boolean removeJoueur(Joueur joueur) {
        return joueurs.remove(joueur);
    }

    public boolean isJoueurPresent(Joueur joueur) {
        return joueurs.contains(joueur);
    }

    public void clear() {
        joueurs.clear();
    }

    public double getMoyenneAge() {
        return joueurs.stream()
                .mapToInt(Joueur::getAge)
                .average()
                .orElse(0.0);
    }

    public int getNombreJoueurs() {
        return joueurs.size();
    }

    @Override
    public String toString() {
        return joueurs.stream()
                .sorted()
                .map(Joueur::toString)
                .collect(Collectors.joining("\n", "Equipe : " + joueurs.size() + " joueurs\n", ""));
    }
}
