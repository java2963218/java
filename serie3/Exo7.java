package org.tpjava2324.longaing_guy.serie3;

public class Exo7 {
    public static void main(String[] args) {
        Equipe equipe = new Equipe();
        equipe.addJoueur(new Joueur("Zinedine", 42));
        equipe.addJoueur(new Joueur("Fabien", 26));
        equipe.addJoueur(new Joueur("Lilian", 26));
        equipe.addJoueur(new Joueur("Bixente", 28));
        equipe.addJoueur(new Joueur("Youri", 30));

        System.out.println(equipe);
        System.out.println("Moyenne d'âge de l'équipe: " + equipe.getMoyenneAge());
    }
}
