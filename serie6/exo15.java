package org.tpjava2324.longaing_guy.serie6;

import org.tpjava2324.longaing_guy.serie6.model.Actor;
import org.tpjava2324.longaing_guy.serie6.model.Movie;

import java.util.AbstractMap;
import java.util.AbstractMap.SimpleEntry;
import java.security.KeyStore.Entry;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class exo15 {

    public static void main(String[] args) {
        ActorsAndMovies actorsAndMovies = new ActorsAndMovies();

        // Nombre de films
        Set<Movie> movies = actorsAndMovies.readMovies();
        System.out.println("Nombre de films est de = " + movies.size());

        // Nombre d'acteurs
        Set<Actor> actors = actorsAndMovies.readActors();
        System.out.println("Nombre d'acteurs est de = " + actors.size());

        // Nombre d'années et année du film le plus vieux et le plus récent
        Set<Integer> anneesSortie = new HashSet<>();
        Map<Integer, Integer> filmsParAnnee = new HashMap<>();
        int vieux = Integer.MAX_VALUE;
        int recent = Integer.MIN_VALUE;
        for (Movie film : movies) {
            int anneeSortie = film.releaseYear();
            anneesSortie.add(anneeSortie);
            filmsParAnnee.put(anneeSortie, filmsParAnnee.getOrDefault(anneeSortie, 0) + 1);

            if (anneeSortie < vieux) {
                vieux = anneeSortie;
            }

            if (anneeSortie > recent) {
                recent = anneeSortie;
            }
        }

        System.out.println("Nombre d'années référencées = " + anneesSortie.size());
        System.out.println("Année de sortie du film le plus vieux = " + vieux);
        System.out.println("Année de sortie du film le plus récent = " + recent);

        // Année avec le plus grand nombre de films
        int anneeMaxFilms = 0;
        int maxFilms = 0;
        for (Map.Entry<Integer, Integer> entry : filmsParAnnee.entrySet()) {
            if (entry.getValue() > maxFilms) {
                maxFilms = entry.getValue();
                anneeMaxFilms = entry.getKey();
            }
        }
        System.out.println("Année avec le plus grand nombre de films = " + anneeMaxFilms + " (" + maxFilms + " films)");

        // Trouver le film avec le plus grand nombre d'acteurs
        String filmLePlusDeActeurs = "";
        int maxNombreActeurs = 0;
        for (Movie film : movies) {
            Set<Actor> acteursDuFilm = film.actors();
            int nombreActeurs = acteursDuFilm.size();
            if (nombreActeurs > maxNombreActeurs) {
                maxNombreActeurs = nombreActeurs;
                filmLePlusDeActeurs = film.title();
            }
        }
        System.out.println("Film avec le plus grand nombre d'acteurs = " + filmLePlusDeActeurs + " (" + maxNombreActeurs
                + " acteurs)");

        // Trouver l'acteur avec le plus grand nombre de films
        Map<Actor, Integer> filmsParActeur = new HashMap<>();
        for (Movie film : movies) {
            for (Actor acteur : film.actors()) {
                filmsParActeur.put(acteur, filmsParActeur.getOrDefault(acteur, 0) + 1);
            }
        }
        Actor acteurLePlusDeFilms = null;
        int maxFilmsParActeur = 0;
        for (Map.Entry<Actor, Integer> entry : filmsParActeur.entrySet()) {
            if (entry.getValue() > maxFilmsParActeur) {
                maxFilmsParActeur = entry.getValue();
                acteurLePlusDeFilms = entry.getKey();
            }
        }

        if (acteurLePlusDeFilms != null) {
            String nom = acteurLePlusDeFilms.firstName() + " " + acteurLePlusDeFilms.lastName();
            System.out.println("Acteur ayant joué dans le plus grand nombre de films = " + nom + " ("
                    + maxFilmsParActeur + " films)");
        }
        // a
        Comparator<Actor> actorComparator = Comparator
                .comparing(Actor::lastName)
                .thenComparing(Actor::firstName);

        // b
        BiFunction<Stream<Actor>, Actor, Stream<Map.Entry<Actor, Actor>>> pairStreamActor = (stream, actor) -> stream
                .filter(a -> !a.equals(actor))
                .map(a -> new AbstractMap.SimpleEntry<>(actor, a)); 
        // c.
        Function<Movie, Stream<Actor>> movieActors = movie -> movie.actors().stream();

        // d. 
        BiFunction<Movie, Actor, Stream<Map.Entry<Actor, Actor>>> actorPairsOfMovie = (movie, actor) -> pairStreamActor
                .apply(movieActors.apply(movie), actor);

        // e. 
        Function<Movie, Stream<Map.Entry<Actor, Actor>>> pairsOfActors = movie -> movieActors.apply(movie)
                .flatMap(actor -> actorPairsOfMovie.apply(movie, actor));

        // f.
        Map<Map.Entry<Actor, Actor>, Long> actorPairCounts = movies.stream()
                .flatMap(pairsOfActors)
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

        // g
        Map.Entry<Map.Entry<Actor, Actor>, Long> mostFrequentPair = actorPairCounts.entrySet().stream()
                .max(Map.Entry.comparingByValue())
                .orElse(null);

        if (mostFrequentPair != null) {
            Actor actor1 = mostFrequentPair.getKey().getKey();
            Actor actor2 = mostFrequentPair.getKey().getValue();
            long frequency = mostFrequentPair.getValue();
            System.out.println("La paire d'acteurs qui a le plus souvent joué ensemble est : " +
                    actor1.firstName() + " " + actor1.lastName() + " et " +
                    actor2.firstName() + " " + actor2.lastName() + ", " + frequency + " fois.");
        }
    
    }
}
