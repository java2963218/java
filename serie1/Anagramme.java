package org.tpjava2324.longaing_guy.serie1;

import java.util.Arrays;

public class Anagramme {


    public static boolean isAnagramme(String s1, String s2) {
        
        char[] array1 = s1.replaceAll("\\s+", "").toLowerCase().toCharArray();
        char[] array2 = s2.replaceAll("\\s+", "").toLowerCase().toCharArray();
        

        Arrays.sort(array1);
        Arrays.sort(array2);
        

        return Arrays.equals(array1, array2);
    }

    public static void main(String[] args) {

        System.out.println("Paul et Ngolo sont-ils anagrammes? " + isAnagramme("Paul et Ngolo", "Ngolo et Paul"));
        System.out.println("Telecom et Galilée sont-ils anagrammes? " + isAnagramme("Telecom et Galilée", "Galilée et Telecom"));
        System.out.println("Bonjour le monde et Hello world sont-ils anagrammes? " + isAnagramme("Bonjour le monde", "Hello world"));

        // 4. Vérification des phrases supplémentaires
        System.out.println("Galilée et égalile sont-ils anagrammes? " + isAnagramme("Galilée et égalile", "égalile et Galilée"));
        System.out.println("Serveur et reveurs sont-ils anagrammes? " + isAnagramme("Serveur et reveurs", "reveurs et Serveur"));
        System.out.println("Boris Vian et Bison ravi sont-ils anagrammes? " + isAnagramme("Boris Vian", "Bison ravi"));
        System.out.println("La foret amazonienne et La zone enorme fanati sont-ils anagrammes? " + isAnagramme("La foret amazonienne", "La zone enorme fanati"));
    }
}
