le programme calcule les factorielles de 10, 13 et 25 en utilisant les méthodes intFactorielle, doubleFactorielle et bigIntFactorielle. 

Pour le factoriel de 25, l'approche int et double ne pas donne pas des résultats corrects, alors que BigInteger donne la reponse correcte. Pour le factoriel de 13,le type int est insuffisant également.