
package org.tpjava2324.longaing_guy.serie1;

public class Palindrome {

    public Palindrome() {
        
    }

    public static boolean palindrome(String s) {
        s = s.replaceAll("[\\W_]+", "").toLowerCase(); 
        int i = 0;
        int j = s.length() - 1; 

        while (i < j) { 
            if (s.charAt(i) != s.charAt(j)) { 
                return false; 
            }
            i++;
            j--;
        }
        return true;
    }

    public static void main(String args[]) {
        String[] phrases = {
                "Rions noir",
                "Esope reste ici et se repose",
                "Elu par cette crapule",
                "Et la marine va venir a Malte",
                "Severe, dissuasive, je vis aussi de reves"
        };
        for (int i = 0; i < phrases.length; i++) {
            boolean isPalindrome = palindrome(phrases[i]);
            System.out.println("\"" + phrases[i] + "\" est un palindrome? " + isPalindrome);
        }
    }
}


