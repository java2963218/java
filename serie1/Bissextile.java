package org.tpjava2324.longaing_guy.serie1;

public class Bissextile {

    public Bissextile() {

    }
    
    public static boolean bissextile (int n) {

        if ((n%4 == 0 && n%100 !=0 )|| (n %400 ==0)){
            return true;
        }else {
            return false;
        }

    }

public static void main(String[] args){
    int annee[] = {1900,1901,1996,2000};
    for (int i=0; i< annee.length; i++){
        
        System.out.println(i + "est elle bissextile ? " +
        Bissextile.bissextile(i));
    }
}
}
