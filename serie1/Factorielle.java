package org.tpjava2324.longaing_guy.serie1;

import java.math.BigInteger;

public class Factorielle {

    public Factorielle(){
        
    }
    public int intFactorielle (int n) {
        int result = 1;
        for (int i = 2 ; i<= n; i++){
            result *=i;
        }
        return result;
    }
    public double doubleFactorielle (int n) {
        double result = 1.0;
        for (int i = 2 ; i<= n; i++){  
            result *=i;
        }
        return result;
    }

    public BigInteger bigInFactorielle (int n) {
        BigInteger result = BigInteger.ONE;
        for (int i = 2 ; i<= n; i++){
            result = result.multiply(BigInteger.valueOf(i));
        }
        return result;
    }

    public static void main(String[] args) {
        Factorielle fact = new Factorielle() ;
        System.out.println("Factorielle de 5 = " +
        fact.intFactorielle(13)) ;
        System.out.println("double Factorielle de 5 = " +
        fact.doubleFactorielle(13)) ;
        System.out.println("BigInt Factorielle de 5 = " +
        fact.bigInFactorielle(13)) ;
    }
    

}