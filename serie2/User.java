
package org.tpjava2324.longaing_guy.serie2;

public class User {
    private String nom;
    private String prenom;
    private double salaire;

    public User(String nom, String prenom, double salaire) {
        this.nom = nom;
        this.prenom = prenom;
        this.salaire = salaire;
    }

    
    public String getNom() { 
        return nom; 
    }
    public String getPrenom() { 
        return prenom; 
    }
    public double getSalaire() { 
        return salaire; 
    }
    
    public void setNom(String nom) { 
        this.nom = nom; 
    }
    public void setPrenom(String prenom) { 
        this.prenom = prenom; 
    }
    public void setSalaire(double salaire) { 
        this.salaire = salaire; 
    }

   
    public void raise(int raise) {
        this.salaire += raise;
    }
    
    
    @Override
    public String toString() {
        return "User: prenom = " + prenom + ", nom = " + nom + ", salaire = " + salaire;
    }

    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        User user = (User) obj;
        return Double.compare(user.salaire, salaire) == 0 &&
               nom.equals(user.nom) &&
               prenom.equals(user.prenom);
    }

    @Override
    public int hashCode() {
        int result = nom.hashCode();
        result = 31 * result + prenom.hashCode();
        result = 31 * result + Double.hashCode(salaire);
        return result;
    }
    
    public int compareTo(User other) {
        int Nom = nom.compareTo(other.nom);
        if (Nom != 0) {

            return Nom;
        }
        int Prenom = prenom.compareTo(other.prenom);
        if (Prenom != 0){ 

            return Prenom;
        }

        return Double.compare(salaire, other.salaire);
    }
}
