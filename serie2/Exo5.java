package org.tpjava2324.longaing_guy.serie2;

public class Exo5 {

    public static void main(String args[]) {

        User u1 = new User("Keuni", "Guy", 5000);
        User u2 = new User("Keuni", "Guy", 5000);
        User u3 = new User("Longaing", "Arnaud", 15000);

        System.out.println(u1);
        System.out.println(u2);
        System.out.println(u3);

        System.out.println("u1 est égal à u2? " + u1.equals(u2));
        System.out.println("Le code hash de u1 = " + u1.hashCode());
        System.out.println("Le code hash de u2 = " + u2.hashCode());
        System.out.println("Le code hash de u3 = " + u3.hashCode());

        System.out.println("Comparaison de u1 et u2 (devrait être 0): " + u1.compareTo(u2));
        System.out.println("Comparaison de u1 et u3 (devrait être un nombre négatif): " + u1.compareTo(u3));
        System.out.println("Comparaison de u3 et u1 (devrait être un nombre positif): " + u3.compareTo(u1));
    
    }
}
