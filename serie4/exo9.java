package org.tpjava2324.longaing_guy.serie4;

import java.util.function.Predicate;

public class exo9 {
    public static void main(String[] args) {
        
        // La chaîne est plus longue que 4 caractères
        Predicate<String> plusLongQueQuatre = s -> s.length() > 4;

        //Chaîne n'est pas vide
        Predicate<String> nonVide = s -> !s.isEmpty();

        //La chaîne commence par J
        Predicate<String> commenceParJ = s -> s.startsWith("J");

        //La chaîne fait 5 caractères de long
        Predicate<String> cinqCaracteresDeLong = s -> s.length() == 5;
        
        // La chaîne commence par J et fait  5 caractères de long
        Predicate<String> commenceParJEtCinqCaracteres = s -> commenceParJ.test(s) && cinqCaracteresDeLong.test(s);

        String test = "Java";
        System.out.println("Java");
        System.out.println("-Est plus longue que quatre : " + plusLongQueQuatre.test(test));
        System.out.println("-Nest pas vide : " + nonVide.test(test));
        System.out.println("-Commence par J : " + commenceParJ.test(test));
        System.out.println("-Fait cinq caractères de long : " + cinqCaracteresDeLong.test(test));
        System.out.println("-Commence par J et fait cinq caractères de long : " + commenceParJEtCinqCaracteres.test(test));
    }
}
