package org.tpjava2324.longaing_guy.serie4;

import java.util.function.BiFunction;
import java.util.function.Function;

public class exo10 {
    
     public static void main(String[] args) {
        Function<String, String> toUpperCase = str -> str == null ? null : str.toUpperCase();

        Function<String, String> identique = Function.identity();
        Function<String, Integer> longueur = str -> str == null ? 0 : str.length();
        Function<String, Boolean> parenthese = str -> str != null && str.contains("()");
        BiFunction<String, String, Integer> index = (str1, str2) -> {
            if (str1 == null || str2 == null) return -1;
            return str1.indexOf(str2);
        };
        Function<String, Integer> index0 = str -> {
            final String fixed = "abcdefghij";
            return str == null ? -1 : fixed.indexOf(str);
        };

        System.out.println("hello : "+ toUpperCase.apply("hello"));
        System.out.println("hello : "+ identique.apply("hello"));
        System.out.println("hello : "+longueur.apply("hello"));
        System.out.println("hello :  "+parenthese.apply("(hello)"));
        System.out.println("() : "+parenthese.apply("()"));
        System.out.println("Bonjour : "+index.apply("Bonjour", "nj"));
        System.out.println("hello : "+index.apply("Hello", "e"));
        System.out.println("abcdefghij : "+index0.apply("fgh")); 
    }
}
