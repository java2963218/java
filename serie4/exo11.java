package org.tpjava2324.longaing_guy.serie4;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class exo11 {
   public static class Main {
    public static void main (String[] args) {
        List<Person> people = Arrays.asList(
            new Person("Arnaud", "Guy", 18),
            new Person("Keuni", "Longaing", 20),
            new Person("Longaing", "Arnaud", 18),
            new Person("Guy", "Keuni", 22)
        );

        Comparator<Person> lastNameComparator = Comparator.comparing(Person::getLastName);

        System.out.println("Avant tri:");
        for (Person person : people) {
            System.out.println(person);
        }

        people.sort(lastNameComparator);

        System.out.println("\nAprès tri par nom de famille:");
        for (Person person : people) {
            System.out.println(person);
        }
    }
}
}