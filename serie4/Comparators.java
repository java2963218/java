package org.tpjava2324.longaing_guy.serie4;

import java.util.Comparator;

public class Comparators {

    Comparator<String> stringLengthComparator = (s1, s2) -> Integer.compare(s1.length(), s2.length());

    Comparator<Person> lastNameComparator = Comparator.comparing(Person::getLastName);

    Comparator<Person> fullNameComparator = Comparator.comparing(Person::getLastName).thenComparing(Person::getFirstName);

    Comparator<Person> fullNameComparatorReversed = fullNameComparator.reversed();

    Comparator<Person> nullsLastComparator = Comparator.comparing(Person::getLastName, Comparator.nullsLast(Comparator.naturalOrder())).thenComparing(Person::getFirstName, Comparator.nullsLast(Comparator.naturalOrder()));
}
