package org.tpjava2324.longaing_guy.serie8;

import java.io.*;
import java.util.List;
import java.util.ArrayList;

public class Exo20 {

    public static class Person implements Serializable {

        private String lastName;
        private String firstName;
        private int age;

        public Person(String lastName, String firstName, int age) {
            this.lastName = lastName;
            this.firstName = firstName;
            this.age = age;
        }

        @Override
        public String toString() {
            return lastName + ", " + firstName + ", " + age;
        }
    }

    public static class PersonOutputStream extends ObjectOutputStream {

        public PersonOutputStream(OutputStream out) throws IOException {
            super(out);
        }

        public void writePeople(List<Person> people) throws IOException {
            for (Person person : people) {
                writeObject(person);
            }
        }
    }

    public static class PersonInputStream extends ObjectInputStream {

        public PersonInputStream(InputStream in) throws IOException {
            super(in);
        }

        public List<Person> readPeople() throws IOException {
            List<Person> people = new ArrayList<>();
            try {
                while(true) {
                    Person person = (Person) readObject();
                    people.add(person);
                }
            } catch (EOFException | ClassNotFoundException e) {
            }
            return people;
        }
    }

    public static void main(String[] args) {
        List<Person> people = new ArrayList<>();
        people.add(new Person("guy", "Arnaud", 30));
        people.add(new Person("Keuni", "Longaing", 25));

        // Écriture des personnes dans un fichier binaire
        ObjectOutputStream oos = null;
        try {
            oos = new PersonOutputStream(new FileOutputStream("org\\tpjava2324\\longaing_guy\\serie8\\people_20.bin"));
            ((PersonOutputStream) oos).writePeople(people);
            System.out.println("Personnes enregistrées avec succès dans le fichier.");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (oos != null) {
                try {
                    oos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        ObjectInputStream ois = null;
        List<Person> loadedPeople = null;
        try {
            ois = new PersonInputStream(new FileInputStream("org\\tpjava2324\\longaing_guy\\serie8\\people_20.bin"));
            loadedPeople = ((PersonInputStream) ois).readPeople();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (ois != null) {
                try {
                    ois.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        if (loadedPeople != null) {
            for (Person person : loadedPeople) {
                System.out.println(person);
            }
        }
    }
}
