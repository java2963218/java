package org.tpjava2324.longaing_guy.serie8;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Exo19 {

    public static byte[] personToBytes(Person person) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        DataOutputStream dos = new DataOutputStream(baos);
        dos.writeUTF(person.getLastName());
        dos.writeUTF(person.getFirstName());
        dos.writeInt(person.getAge());
        dos.flush();
        return baos.toByteArray();
    }

    public static void savePersonToFile(Person person, String filename) throws IOException {
        byte[] personBytes = personToBytes(person);
        FileOutputStream fos = new FileOutputStream(filename, true);
        fos.write(personBytes);
        fos.close();
    }

    public static List<Person> readPeopleFromFile(String filename) throws IOException {
        List<Person> people = new ArrayList<>();
        FileInputStream fis = new FileInputStream(filename);
        DataInputStream dis = new DataInputStream(fis);
        while (fis.available() > 0) {
            String lastName = dis.readUTF();
            String firstName = dis.readUTF();
            int age = dis.readInt();
            people.add(new Person(lastName, firstName, age));
        }
        dis.close();
        fis.close();
        return people;
    }

    public static void main(String[] args) throws IOException {
        String fileName = "org\\tpjava2324\\longaing_guy\\serie8\\people.bin";
        List<Person> peopleToWrite = new ArrayList<>();
        peopleToWrite.add(new Person("Guy", "Arnaud", 24));
        peopleToWrite.add(new Person("Longaing", "Keuni", 25));
        peopleToWrite.add(new Person("Keuni", "Arnaud", 18));

        for (Person person : peopleToWrite) {
            savePersonToFile(person, fileName);
        }

        List<Person> readPeople = readPeopleFromFile(fileName);
        System.out.println("Personnes lues depuis le fichier binaire :");
        for (Person person : readPeople) {
            System.out.println(person);
        }
    }
}
