package org.tpjava2324.longaing_guy.serie8;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class PersonOutputStream extends OutputStream {
    private final DataOutputStream dos;

    public PersonOutputStream(OutputStream out) {
        this.dos = new DataOutputStream(out);
    }

    public void writePerson(Person person) throws IOException {
        dos.writeUTF(person.getLastName());
        dos.writeUTF(person.getFirstName());
        dos.writeInt(person.getAge());
    }

    @Override
    public void write(int b) throws IOException {
        dos.write(b);
    }

}
