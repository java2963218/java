package org.tpjava2324.longaing_guy.serie8;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class PersonReader {
    public static List<Person> read(String fileName) {
        List<Person> people = new ArrayList<>();
        Path path = Paths.get(fileName);

        try (BufferedReader reader = Files.newBufferedReader(path, StandardCharsets.UTF_8)) {
            people = reader.lines()
                    .filter(line -> !(line.trim().isEmpty() || line.startsWith("#")))
                    .flatMap(line -> {
                        try {
                            return Stream.of(parsePerson(line));
                        } catch (Exception e) {
                            return Stream.empty();
                        }
                    })
                    .collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return people;
    }

    private static Person parsePerson(String line) {
        String[] parts = line.split(",\\s*");
        if (parts.length != 3) {
            throw new IllegalArgumentException("La ligne n'est pas au format attendu : " + line);
        }
        return new Person(parts[0], parts[1], Integer.parseInt(parts[2]));
    }
}