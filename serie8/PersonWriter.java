package org.tpjava2324.longaing_guy.serie8;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class PersonWriter {
    public static void write(List<Person> people, String fileName) throws IOException {
        try (BufferedWriter writer = Files.newBufferedWriter(Paths.get(fileName))) {
            for (Person person : people) {
                writer.write(person.toString());
                writer.newLine();
            }
        }
    }
}
