question 1 : 
En résumé, l'interface Stream étend l'interface AutoCloseable, ce qui signifie que tout objet Stream peut être utilisé dans un bloc try-with-resources pour garantir sa fermeture automatique une fois que toutes les opérations sur le Stream sont terminées.
question 3 : 
Lors de l'opération flatMap(), les Streams vides sont simplement ignorés