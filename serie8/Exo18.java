package org.tpjava2324.longaing_guy.serie8;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class Exo18 {
    public static void main(String[] args) throws IOException {
        List<Person> people = Arrays.asList(
                new Person("Jacquet", "Aimé", 79),
                new Person("Deschamps", "Didier", 51),
                new Person("Thuram", "Lilian", 48),
                new Person("Barthez", "Fabien", 49),
                new Person("Zidane", "Zinedine", 47)
        );

        String fileName = "org\\tpjava2324\\longaing_guy\\serie8\\Note.txt";
        PersonWriter.write(people, fileName);
        System.out.println(" Lecture par la méthode : ");
        List<Person> readPeople = PersonReader.read(fileName);
        readPeople.forEach(System.out::println);
    }
}
