package org.tpjava2324.longaing_guy.serie8;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;

public class PersonInputStream extends InputStream {
    private final DataInputStream dis;

    public PersonInputStream(InputStream in) {
        this.dis = new DataInputStream(in);
    }

    public Person readPerson() throws IOException {
        String lastName = dis.readUTF();
        String firstName = dis.readUTF();
        int age = dis.readInt();
        return new Person(lastName, firstName, age);
    }

    @Override
    public int read() throws IOException {
        return dis.read();
    }


}
